# winform-starter-with-config

A skeleton Windows Forms C# Program with:

  * Menus with starter common menu layout and menu methods.
  * Utility class to write to Rich Text Box console.
  * Hooks for Help and About.
  * Mechanism for parsing arguments.
  * JSON based configuration.
  * Options dialog for managing configuration.
 
