﻿using System;
using System.Drawing;
using System.Windows.Forms;

//-------------------------------------------------------------------------------------------------

namespace Utilities
{
    //---------------------------------------------------------------------------------------------

    public static class RtbConsole
    {
        private static RichTextBox _textArea;

        //-----------------------------------------------------------------------------------------

        public static void Setup(RichTextBox textBox)
        {
            _textArea = textBox;

            textBox.Font = new Font(FontFamily.GenericMonospace, 10, FontStyle.Regular);
        } // TbConsole

        //-----------------------------------------------------------------------------------------

        public static void Write(string text)
        {
            _textArea.AppendText(text);
        } // Write

        //-----------------------------------------------------------------------------------------

        public static void WriteLine(string text = null)
        {
            if (text != null)
                _textArea.AppendText(text);

            _textArea.AppendText(Environment.NewLine);
        } // WriteLine

        //-----------------------------------------------------------------------------------------

        public static void DisplayLine(string line = null)
        {
            if (line != null)
                _textArea.AppendText(line);

            _textArea.AppendText(Environment.NewLine);
        } // DisplayLine

        //-----------------------------------------------------------------------------------------

        public static void Clear()
        {
            _textArea.Clear();
            _textArea.Invalidate();
        } // Clear

        //-----------------------------------------------------------------------------------------

    } // class RTbConsole

    //---------------------------------------------------------------------------------------------

} // namespace Utilities

//-------------------------------------------------------------------------------------------------
