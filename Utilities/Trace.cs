﻿using System.IO;

//-------------------------------------------------------------------------------------------------

namespace Utilities
{
    //---------------------------------------------------------------------------------------------

    public static class Trace
    {

        private static StreamWriter _sw;
        private const string FilePath = @"C:\TEMP\SkelTrace.txt";

        //-----------------------------------------------------------------------------------------

        public static void Init()
        {
            if (_sw == null)
            {
                _sw = new StreamWriter(FilePath, true);
            }
        } // Init

        //-----------------------------------------------------------------------------------------

        public static void Write(string text)
        {
            if (_sw == null)
            {
                Init();
            }

            if (_sw != null)
            {
                _sw.Write(text);
                _sw.Flush();
            }
        } // Write

        //-----------------------------------------------------------------------------------------

        public static void WriteLine(string text)
        {
            if (_sw == null)
            {
                Init();
            }

            if (_sw != null)
            {
                _sw.WriteLine(text);
                _sw.Flush();
            }
        } // WriteLine

        //-----------------------------------------------------------------------------------------

        public static void Close()
        {
            if (_sw != null)
            {
                _sw.Close();
                _sw = null;
            }
        } // Close

        //-----------------------------------------------------------------------------------------

    } // class Trace

    //---------------------------------------------------------------------------------------------

} // namespace Utilities

//-----------------------------------------------------------------------------
