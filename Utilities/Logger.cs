﻿using System;
using System.IO;

//-----------------------------------------------------------------------------

namespace Utilities
{
    public static class Logger
    {
        private static StreamWriter _sw;
        private const string FilePathTemplate = @"C:\TEMP\{0}_Skel.log";
        private static string _filePath;

        //---------------------------------------------------------------------

        public static void Init()
        {
            if (_sw == null)
            {
                _filePath = string.Format(FilePathTemplate, DateTime.Now.ToString("yyyyMMdd"));
                try
                {
                    _sw = new StreamWriter(_filePath, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Attempt to open StreamWriter ({0}) failed - {1}", _filePath, ex);
                }

            }
        } // Init

        //---------------------------------------------------------------------

        public static void Log(string text)
        {
            if (_sw == null)
            {
                Init();
            }
            if (_sw != null)
            {
                _sw.WriteLine("[{0}] {1}", DateTime.Now, text);
                _sw.Flush();
            }

        } // Log

        //---------------------------------------------------------------------

        public static void Close()
        {
            if (_sw != null)
            {
                _sw.Close();
                _sw = null;
            }

        } // Close

        //---------------------------------------------------------------------

    } // class Logger

    //-------------------------------------------------------------------------

} // namespace Utilities

//-----------------------------------------------------------------------------
