﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text.RegularExpressions;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Skel
{
    public class Configuration
    {

        public Color SolidBrushColour { get; set; }
        public Color DottedBrushColour { get; set; }
        public HatchStyle HatchStyle { get; set; }
        public float BrushThickness { get; set; }

        public string WorkFolder { get; set; }

        public const string ConfigFile = @"Skel.json";

        public static string ConfigFilePath => $@"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}/{ConfigFile}";

        //-----------------------------------------------------------------------------------------

        public static Configuration Defaults = new Configuration
        {
            SolidBrushColour = Color.DarkRed,
            DottedBrushColour = Color.BlueViolet,
            HatchStyle = HatchStyle.BackwardDiagonal,
            BrushThickness = 0.5F,
            WorkFolder = @"C:\Temp"
        };

        //-----------------------------------------------------------------------------------------

        public Configuration()
        {

        }

        //-----------------------------------------------------------------------------------------

        public static Configuration Load()
        {
            string jsonData;

            try
            {
                jsonData = File.ReadAllText(ConfigFilePath);
            }
            catch (FileNotFoundException)
            {

                jsonData = String.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception($@"Caught - {ex.ToString()}");
            }

            var config = new Configuration();

            if (jsonData.Length > 0)
            {
                JObject jsonObj = JObject.Parse(jsonData);

                Console.WriteLine(jsonObj.ToString());

                config.Set(jsonObj);
            }
            else
            {
                config = Configuration.Defaults;
            }

            return config;
        }

        //-----------------------------------------------------------------------------------------

        public static void Save(Configuration config)
        {
            var serializedObjectJson = JsonConvert.SerializeObject(config);

            Console.WriteLine(serializedObjectJson);

            var sw = new StreamWriter(ConfigFilePath);

            sw.Write(serializedObjectJson);

            sw.Close();
        }

        //-----------------------------------------------------------------------------------------

        private void Set(JObject jsonData)
        {
            var solidBrushColourJson = (string)jsonData["SolidBrushColour"];

            Match match = Regex.Match(solidBrushColourJson, "^([0-9]*), ([0-9]*), ([0-9]*)$", RegexOptions.IgnoreCase);

            if (match.Success)
            {
                var red = Convert.ToInt32(match.Groups[1].Value);
                var green = Convert.ToInt32(match.Groups[2].Value);
                var blue = Convert.ToInt32(match.Groups[3].Value);

                SolidBrushColour = Color.FromArgb(red, green, blue);
            } else
                SolidBrushColour = Color.FromName(solidBrushColourJson);

            var dottedBrushColourJson = (string)jsonData["DottedBrushColour"];

            match = Regex.Match(dottedBrushColourJson, "^([0-9]*), ([0-9]*), ([0-9]*)$", RegexOptions.IgnoreCase);

            if (match.Success)
            {
                var red = Convert.ToInt32(match.Groups[1].Value);
                var green = Convert.ToInt32(match.Groups[2].Value);
                var blue = Convert.ToInt32(match.Groups[3].Value);

                DottedBrushColour = Color.FromArgb(red, green, blue);
            }
            else
                DottedBrushColour = Color.FromName(dottedBrushColourJson);


            HatchStyle = GetHatchStyle((int)jsonData["HatchStyle"]);

            try
            {
                BrushThickness = (float)jsonData["BrushThickness"];                
            }
            catch
            {
                BrushThickness = Defaults.BrushThickness;
            }
        }

        //-----------------------------------------------------------------------------------------

        private HatchStyle GetHatchStyle(int hatchStyleEnum)
        {
            HatchStyle hs;

            switch (hatchStyleEnum)
            {
                case 0:
                    hs = HatchStyle.DiagonalBrick;
                    break;
                case 1:
                     hs = HatchStyle.Percent20;
                     break;
                case 2:
                    hs = HatchStyle.Percent20;
                    break;
                case 3:
                    hs = HatchStyle.BackwardDiagonal;
                    break;
                default:
                    hs = HatchStyle.Horizontal;
                    break;
            }

            return hs;
        }

        //-----------------------------------------------------------------------------------------

    }
}


/*
// read JSON directly from a file
using (StreamReader file = File.OpenText(@"c:\videogames.json"))
using (JsonTextReader reader = new JsonTextReader(file))
{
  JObject o2 = (JObject) JToken.ReadFrom(reader);
}
*/

/*
public void LoadJson()
{
    using (StreamReader r = new StreamReader("file.json"))
    {
        string json = r.ReadToEnd();
        List<Item> items = JsonConvert.DeserializeObject<List<Item>>(json);
    }
}

public class Item
{
    public int millis;
    public string stamp;
    public DateTime datetime;
    public string light;
    public float temp;
    public float vcc;
}
*/

/*
var serializer = new JavaScriptSerializer();

serializer.RegisterConverters(new[] { new DynamicJsonConverter() });

string json = File.ReadAllText(ConfigFilePath);

var config = serializer.Deserialize(json, typeof(Configuration));

*/


/*
JsonWriter jw = new JsonTextWriter(streamWriter)
  JsonSerializer serializer = new JsonSerializer();
  // serializer.Serialize(jw, Foo);

  public static FooClass Foo = new FooClass();
    [Serializable]
    public class FooClass : System.Runtime.Serialization.ISerializable
    {
        public FooClass()
        {

        }
        public static int foo1 = 1;
        public static int foo2 = 2;
        public static string foo3 = "test";

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("static.foo1", FooClass.foo1, typeof(int));
            info.AddValue("static.foo2", FooClass.foo2, typeof(int));
            info.AddValue("static.foo3", FooClass.foo3, typeof(string));
        }

        public FooClass(SerializationInfo info, StreamingContext context)
        {
            FooClass.foo1 = info.GetInt32("static.foo1");
            FooClass.foo2 = info.GetInt32("static.foo2");
            FooClass.foo3 = info.GetString("static.foo3");
        }
    }
    ...
    serializer.Serialize(jw, Foo);

*/

/*
public static class Colors
{
    private static readonly Dictionary<string, Color> dictionary =
        typeof(Color).GetProperties(BindingFlags.Public | 
                                    BindingFlags.Static)
                     .Where(prop => prop.PropertyType == typeof(Color))
                     .ToDictionary(prop => prop.Name,
                                   prop => (Color) prop.GetValue(null, null)));

    public static Color FromName(string name)
    {
        // Adjust behaviour for lookup failure etc
        return dictionary[name];
    }
}
That will be relatively slow for the first lookup (while it uses reflection to find all the properties) but should be very quick after that.

If you want it to be case-insensitive, you can pass in something like StringComparer.OrdinalIgnoreCase as an extra argument in the ToDictionary call. You can easily add TryParse etc methods should you wish.
*/

/*
 public static Color FromName(String name)
    {
        var color_props= typeof(Colors).GetProperties();
        foreach (var c in color_props)
            if (name.Equals(c.Name, StringComparison.OrdinalIgnoreCase))
                return (Color)c.GetValue(new Color(), null);
        return Colors.Transparent;
    }
*/

// http://www.dreamincode.net/forums/topic/67275-the-wonders-of-systemdrawinggraphics/
// http://etutorials.org/Programming/visual-c-sharp/Part+III+Programming+Windows+Forms/Chapter+14+GDI/Using+Brushes/



