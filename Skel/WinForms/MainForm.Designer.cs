﻿namespace Skel.WinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Exit = new System.Windows.Forms.Button();
            this.Btn_DoSomething = new System.Windows.Forms.Button();
            this.Rtb_Console = new System.Windows.Forms.RichTextBox();
            this.Menu_TopLevel = new System.Windows.Forms.MenuStrip();
            this.Menu_File = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_New = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_Close = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_File_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_File_Recent = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_File_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Undo = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Redo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Edit_Cut = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.advancedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Search = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Search_Find = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Search_FindInFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Search_FindNext = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Search_FindPrevious = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Search_Replace = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Search_GoTo = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_View = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Tools = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Tools_Explore = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Tools_ViewPdf = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Tools_ViewTiff = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Tools_Options = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Help_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Help_About = new System.Windows.Forms.ToolStripMenuItem();
            this.Lbl_WrkFolder = new System.Windows.Forms.Label();
            this.Txt_WorkFolder = new System.Windows.Forms.TextBox();
            this.Lbl_Main_Version = new System.Windows.Forms.Label();
            this.Lbl_Command = new System.Windows.Forms.Label();
            this.Txt_Cmd = new System.Windows.Forms.TextBox();
            this.Menu_TopLevel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_Exit
            // 
            this.Btn_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Exit.Location = new System.Drawing.Point(923, 543);
            this.Btn_Exit.Name = "Btn_Exit";
            this.Btn_Exit.Size = new System.Drawing.Size(75, 23);
            this.Btn_Exit.TabIndex = 0;
            this.Btn_Exit.Text = "Exit";
            this.Btn_Exit.UseVisualStyleBackColor = true;
            this.Btn_Exit.Click += new System.EventHandler(this.Btn_Exit_Click);
            // 
            // Btn_DoSomething
            // 
            this.Btn_DoSomething.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_DoSomething.Location = new System.Drawing.Point(842, 543);
            this.Btn_DoSomething.Name = "Btn_DoSomething";
            this.Btn_DoSomething.Size = new System.Drawing.Size(75, 23);
            this.Btn_DoSomething.TabIndex = 1;
            this.Btn_DoSomething.Text = "Do It";
            this.Btn_DoSomething.UseVisualStyleBackColor = true;
            this.Btn_DoSomething.Click += new System.EventHandler(this.Btn_DoSomething_Click);
            // 
            // Rtb_Console
            // 
            this.Rtb_Console.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Rtb_Console.Location = new System.Drawing.Point(12, 57);
            this.Rtb_Console.Name = "Rtb_Console";
            this.Rtb_Console.ReadOnly = true;
            this.Rtb_Console.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.Rtb_Console.Size = new System.Drawing.Size(986, 480);
            this.Rtb_Console.TabIndex = 2;
            this.Rtb_Console.Text = "";
            // 
            // Menu_TopLevel
            // 
            this.Menu_TopLevel.BackColor = System.Drawing.SystemColors.Window;
            this.Menu_TopLevel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_File,
            this.Menu_Edit,
            this.Menu_Search,
            this.Menu_View,
            this.Menu_Tools,
            this.Menu_Help});
            this.Menu_TopLevel.Location = new System.Drawing.Point(0, 0);
            this.Menu_TopLevel.Name = "Menu_TopLevel";
            this.Menu_TopLevel.Size = new System.Drawing.Size(1010, 24);
            this.Menu_TopLevel.TabIndex = 3;
            this.Menu_TopLevel.Text = "menuStrip1";
            // 
            // Menu_File
            // 
            this.Menu_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_File_New,
            this.Menu_File_Open,
            this.Menu_File_Close,
            this.toolStripSeparator4,
            this.Menu_File_Save,
            this.Menu_File_SaveAs,
            this.toolStripSeparator5,
            this.Menu_File_Recent,
            this.toolStripSeparator6,
            this.Menu_File_Exit});
            this.Menu_File.Name = "Menu_File";
            this.Menu_File.Size = new System.Drawing.Size(37, 20);
            this.Menu_File.Text = "File";
            // 
            // Menu_File_New
            // 
            this.Menu_File_New.Name = "Menu_File_New";
            this.Menu_File_New.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.Menu_File_New.Size = new System.Drawing.Size(180, 22);
            this.Menu_File_New.Text = "New";
            this.Menu_File_New.Click += new System.EventHandler(this.Menu_File_New_Click);
            // 
            // Menu_File_Open
            // 
            this.Menu_File_Open.Name = "Menu_File_Open";
            this.Menu_File_Open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.Menu_File_Open.Size = new System.Drawing.Size(180, 22);
            this.Menu_File_Open.Text = "Open";
            this.Menu_File_Open.Click += new System.EventHandler(this.Menu_File_Open_Click);
            // 
            // Menu_File_Close
            // 
            this.Menu_File_Close.Enabled = false;
            this.Menu_File_Close.Name = "Menu_File_Close";
            this.Menu_File_Close.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.Menu_File_Close.Size = new System.Drawing.Size(180, 22);
            this.Menu_File_Close.Text = "Close";
            this.Menu_File_Close.Click += new System.EventHandler(this.Menu_File_Close_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(177, 6);
            // 
            // Menu_File_Save
            // 
            this.Menu_File_Save.Enabled = false;
            this.Menu_File_Save.Name = "Menu_File_Save";
            this.Menu_File_Save.Size = new System.Drawing.Size(180, 22);
            this.Menu_File_Save.Text = "Save";
            this.Menu_File_Save.Click += new System.EventHandler(this.Menu_File_Save_Click);
            // 
            // Menu_File_SaveAs
            // 
            this.Menu_File_SaveAs.Enabled = false;
            this.Menu_File_SaveAs.Name = "Menu_File_SaveAs";
            this.Menu_File_SaveAs.Size = new System.Drawing.Size(180, 22);
            this.Menu_File_SaveAs.Text = "Save As";
            this.Menu_File_SaveAs.Click += new System.EventHandler(this.Menu_File_SaveAs_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(177, 6);
            // 
            // Menu_File_Recent
            // 
            this.Menu_File_Recent.Enabled = false;
            this.Menu_File_Recent.Name = "Menu_File_Recent";
            this.Menu_File_Recent.Size = new System.Drawing.Size(180, 22);
            this.Menu_File_Recent.Text = "Recent";
            this.Menu_File_Recent.Click += new System.EventHandler(this.Menu_File_Recent_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(177, 6);
            // 
            // Menu_File_Exit
            // 
            this.Menu_File_Exit.Name = "Menu_File_Exit";
            this.Menu_File_Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.Menu_File_Exit.Size = new System.Drawing.Size(180, 22);
            this.Menu_File_Exit.Text = "Exit";
            this.Menu_File_Exit.Click += new System.EventHandler(this.Menu_File_Exit_Click);
            // 
            // Menu_Edit
            // 
            this.Menu_Edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Edit_Undo,
            this.Menu_Edit_Redo,
            this.toolStripSeparator2,
            this.Menu_Edit_Cut,
            this.Menu_Edit_Copy,
            this.Menu_Edit_Paste,
            this.Menu_Edit_Delete,
            this.toolStripSeparator3,
            this.advancedToolStripMenuItem});
            this.Menu_Edit.Name = "Menu_Edit";
            this.Menu_Edit.Size = new System.Drawing.Size(39, 20);
            this.Menu_Edit.Text = "Edit";
            // 
            // Menu_Edit_Undo
            // 
            this.Menu_Edit_Undo.Enabled = false;
            this.Menu_Edit_Undo.Name = "Menu_Edit_Undo";
            this.Menu_Edit_Undo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.Menu_Edit_Undo.Size = new System.Drawing.Size(180, 22);
            this.Menu_Edit_Undo.Text = "Undo";
            this.Menu_Edit_Undo.Click += new System.EventHandler(this.Menu_Edit_Undo_Click);
            // 
            // Menu_Edit_Redo
            // 
            this.Menu_Edit_Redo.Enabled = false;
            this.Menu_Edit_Redo.Name = "Menu_Edit_Redo";
            this.Menu_Edit_Redo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.Menu_Edit_Redo.Size = new System.Drawing.Size(180, 22);
            this.Menu_Edit_Redo.Text = "Redo";
            this.Menu_Edit_Redo.Click += new System.EventHandler(this.Menu_Edit_Redo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // Menu_Edit_Cut
            // 
            this.Menu_Edit_Cut.Enabled = false;
            this.Menu_Edit_Cut.Name = "Menu_Edit_Cut";
            this.Menu_Edit_Cut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.Menu_Edit_Cut.Size = new System.Drawing.Size(180, 22);
            this.Menu_Edit_Cut.Text = "Cut";
            this.Menu_Edit_Cut.Click += new System.EventHandler(this.Menu_Edit_Cut_Click);
            // 
            // Menu_Edit_Copy
            // 
            this.Menu_Edit_Copy.Enabled = false;
            this.Menu_Edit_Copy.Name = "Menu_Edit_Copy";
            this.Menu_Edit_Copy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.Menu_Edit_Copy.Size = new System.Drawing.Size(180, 22);
            this.Menu_Edit_Copy.Text = "Copy";
            this.Menu_Edit_Copy.Click += new System.EventHandler(this.Menu_Edit_Copy_Click);
            // 
            // Menu_Edit_Paste
            // 
            this.Menu_Edit_Paste.Enabled = false;
            this.Menu_Edit_Paste.Name = "Menu_Edit_Paste";
            this.Menu_Edit_Paste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.Menu_Edit_Paste.Size = new System.Drawing.Size(180, 22);
            this.Menu_Edit_Paste.Text = "Paste";
            this.Menu_Edit_Paste.Click += new System.EventHandler(this.Menu_Edit_Paste_Click);
            // 
            // Menu_Edit_Delete
            // 
            this.Menu_Edit_Delete.Enabled = false;
            this.Menu_Edit_Delete.Name = "Menu_Edit_Delete";
            this.Menu_Edit_Delete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.Menu_Edit_Delete.Size = new System.Drawing.Size(180, 22);
            this.Menu_Edit_Delete.Text = "Delete";
            this.Menu_Edit_Delete.Click += new System.EventHandler(this.Menu_Edit_Delete_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(177, 6);
            // 
            // advancedToolStripMenuItem
            // 
            this.advancedToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formatDocumentToolStripMenuItem});
            this.advancedToolStripMenuItem.Name = "advancedToolStripMenuItem";
            this.advancedToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.advancedToolStripMenuItem.Text = "Advanced";
            // 
            // formatDocumentToolStripMenuItem
            // 
            this.formatDocumentToolStripMenuItem.Enabled = false;
            this.formatDocumentToolStripMenuItem.Name = "formatDocumentToolStripMenuItem";
            this.formatDocumentToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.formatDocumentToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.formatDocumentToolStripMenuItem.Text = "Format Document";
            // 
            // Menu_Search
            // 
            this.Menu_Search.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Search_Find,
            this.Menu_Search_FindInFiles,
            this.Menu_Search_FindNext,
            this.Menu_Search_FindPrevious,
            this.Menu_Search_Replace,
            this.toolStripSeparator8,
            this.Menu_Search_GoTo});
            this.Menu_Search.Name = "Menu_Search";
            this.Menu_Search.Size = new System.Drawing.Size(54, 20);
            this.Menu_Search.Text = "Search";
            // 
            // Menu_Search_Find
            // 
            this.Menu_Search_Find.Enabled = false;
            this.Menu_Search_Find.Name = "Menu_Search_Find";
            this.Menu_Search_Find.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.Menu_Search_Find.Size = new System.Drawing.Size(217, 22);
            this.Menu_Search_Find.Text = "Find...";
            // 
            // Menu_Search_FindInFiles
            // 
            this.Menu_Search_FindInFiles.Enabled = false;
            this.Menu_Search_FindInFiles.Name = "Menu_Search_FindInFiles";
            this.Menu_Search_FindInFiles.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.Menu_Search_FindInFiles.Size = new System.Drawing.Size(217, 22);
            this.Menu_Search_FindInFiles.Text = "Find in FIles...";
            // 
            // Menu_Search_FindNext
            // 
            this.Menu_Search_FindNext.Enabled = false;
            this.Menu_Search_FindNext.Name = "Menu_Search_FindNext";
            this.Menu_Search_FindNext.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.Menu_Search_FindNext.Size = new System.Drawing.Size(217, 22);
            this.Menu_Search_FindNext.Text = "Find Next";
            // 
            // Menu_Search_FindPrevious
            // 
            this.Menu_Search_FindPrevious.Enabled = false;
            this.Menu_Search_FindPrevious.Name = "Menu_Search_FindPrevious";
            this.Menu_Search_FindPrevious.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this.Menu_Search_FindPrevious.Size = new System.Drawing.Size(217, 22);
            this.Menu_Search_FindPrevious.Text = "Find Previous";
            // 
            // Menu_Search_Replace
            // 
            this.Menu_Search_Replace.Enabled = false;
            this.Menu_Search_Replace.Name = "Menu_Search_Replace";
            this.Menu_Search_Replace.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.Menu_Search_Replace.Size = new System.Drawing.Size(217, 22);
            this.Menu_Search_Replace.Text = "Replace";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(214, 6);
            // 
            // Menu_Search_GoTo
            // 
            this.Menu_Search_GoTo.Enabled = false;
            this.Menu_Search_GoTo.Name = "Menu_Search_GoTo";
            this.Menu_Search_GoTo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.Menu_Search_GoTo.Size = new System.Drawing.Size(217, 22);
            this.Menu_Search_GoTo.Text = "Go to...";
            // 
            // Menu_View
            // 
            this.Menu_View.Name = "Menu_View";
            this.Menu_View.Size = new System.Drawing.Size(44, 20);
            this.Menu_View.Text = "View";
            // 
            // Menu_Tools
            // 
            this.Menu_Tools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Tools_Explore,
            this.Menu_Tools_ViewPdf,
            this.Menu_Tools_ViewTiff,
            this.toolStripSeparator7,
            this.Menu_Tools_Options});
            this.Menu_Tools.Name = "Menu_Tools";
            this.Menu_Tools.Size = new System.Drawing.Size(47, 20);
            this.Menu_Tools.Text = "Tools";
            // 
            // Menu_Tools_Explore
            // 
            this.Menu_Tools_Explore.Name = "Menu_Tools_Explore";
            this.Menu_Tools_Explore.Size = new System.Drawing.Size(180, 22);
            this.Menu_Tools_Explore.Text = "Explore";
            this.Menu_Tools_Explore.Click += new System.EventHandler(this.Menu_Tools_Explore_Click);
            // 
            // Menu_Tools_ViewPdf
            // 
            this.Menu_Tools_ViewPdf.Name = "Menu_Tools_ViewPdf";
            this.Menu_Tools_ViewPdf.Size = new System.Drawing.Size(180, 22);
            this.Menu_Tools_ViewPdf.Text = "View PDF";
            this.Menu_Tools_ViewPdf.Click += new System.EventHandler(this.Menu_Tools_ViewPdf_Click);
            // 
            // Menu_Tools_ViewTiff
            // 
            this.Menu_Tools_ViewTiff.Name = "Menu_Tools_ViewTiff";
            this.Menu_Tools_ViewTiff.Size = new System.Drawing.Size(180, 22);
            this.Menu_Tools_ViewTiff.Text = "View TIFF";
            this.Menu_Tools_ViewTiff.Click += new System.EventHandler(this.Menu_Tools_ViewTiff_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(177, 6);
            // 
            // Menu_Tools_Options
            // 
            this.Menu_Tools_Options.Name = "Menu_Tools_Options";
            this.Menu_Tools_Options.Size = new System.Drawing.Size(180, 22);
            this.Menu_Tools_Options.Text = "Options";
            this.Menu_Tools_Options.Click += new System.EventHandler(this.Menu_Tools_Options_Click);
            // 
            // Menu_Help
            // 
            this.Menu_Help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Help_Help,
            this.toolStripSeparator1,
            this.Menu_Help_About});
            this.Menu_Help.Name = "Menu_Help";
            this.Menu_Help.Size = new System.Drawing.Size(44, 20);
            this.Menu_Help.Text = "Help";
            // 
            // Menu_Help_Help
            // 
            this.Menu_Help_Help.Name = "Menu_Help_Help";
            this.Menu_Help_Help.Size = new System.Drawing.Size(180, 22);
            this.Menu_Help_Help.Text = "Help";
            this.Menu_Help_Help.Click += new System.EventHandler(this.Menu_Help_Help_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // Menu_Help_About
            // 
            this.Menu_Help_About.Name = "Menu_Help_About";
            this.Menu_Help_About.Size = new System.Drawing.Size(180, 22);
            this.Menu_Help_About.Text = "About";
            this.Menu_Help_About.Click += new System.EventHandler(this.Menu_Help_About_Click);
            // 
            // Lbl_WrkFolder
            // 
            this.Lbl_WrkFolder.AutoSize = true;
            this.Lbl_WrkFolder.Location = new System.Drawing.Point(12, 34);
            this.Lbl_WrkFolder.Name = "Lbl_WrkFolder";
            this.Lbl_WrkFolder.Size = new System.Drawing.Size(65, 13);
            this.Lbl_WrkFolder.TabIndex = 0;
            this.Lbl_WrkFolder.Text = "Work Folder";
            // 
            // Txt_WorkFolder
            // 
            this.Txt_WorkFolder.Location = new System.Drawing.Point(83, 31);
            this.Txt_WorkFolder.Name = "Txt_WorkFolder";
            this.Txt_WorkFolder.ReadOnly = true;
            this.Txt_WorkFolder.Size = new System.Drawing.Size(372, 20);
            this.Txt_WorkFolder.TabIndex = 0;
            this.Txt_WorkFolder.TabStop = false;
            // 
            // Lbl_Main_Version
            // 
            this.Lbl_Main_Version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl_Main_Version.AutoSize = true;
            this.Lbl_Main_Version.BackColor = System.Drawing.SystemColors.Window;
            this.Lbl_Main_Version.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Main_Version.Location = new System.Drawing.Point(889, 5);
            this.Lbl_Main_Version.Name = "Lbl_Main_Version";
            this.Lbl_Main_Version.Size = new System.Drawing.Size(86, 13);
            this.Lbl_Main_Version.TabIndex = 24;
            this.Lbl_Main_Version.Text = "Version N.N.N.N";
            this.Lbl_Main_Version.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Lbl_Command
            // 
            this.Lbl_Command.AutoSize = true;
            this.Lbl_Command.Location = new System.Drawing.Point(461, 34);
            this.Lbl_Command.Name = "Lbl_Command";
            this.Lbl_Command.Size = new System.Drawing.Size(54, 13);
            this.Lbl_Command.TabIndex = 25;
            this.Lbl_Command.Text = "Command";
            // 
            // Txt_Cmd
            // 
            this.Txt_Cmd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Cmd.Location = new System.Drawing.Point(521, 31);
            this.Txt_Cmd.Name = "Txt_Cmd";
            this.Txt_Cmd.ReadOnly = true;
            this.Txt_Cmd.Size = new System.Drawing.Size(325, 20);
            this.Txt_Cmd.TabIndex = 26;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 578);
            this.Controls.Add(this.Txt_Cmd);
            this.Controls.Add(this.Lbl_Command);
            this.Controls.Add(this.Lbl_Main_Version);
            this.Controls.Add(this.Txt_WorkFolder);
            this.Controls.Add(this.Lbl_WrkFolder);
            this.Controls.Add(this.Rtb_Console);
            this.Controls.Add(this.Btn_DoSomething);
            this.Controls.Add(this.Btn_Exit);
            this.Controls.Add(this.Menu_TopLevel);
            this.MainMenuStrip = this.Menu_TopLevel;
            this.Name = "MainForm";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Menu_TopLevel.ResumeLayout(false);
            this.Menu_TopLevel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_Exit;
        private System.Windows.Forms.Button Btn_DoSomething;
        private System.Windows.Forms.RichTextBox Rtb_Console;
        private System.Windows.Forms.MenuStrip Menu_TopLevel;
        private System.Windows.Forms.ToolStripMenuItem Menu_File;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_New;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Open;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Close;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Save;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_SaveAs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Recent;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Exit;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Undo;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Redo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Cut;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Copy;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Paste;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Delete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem Menu_View;
        private System.Windows.Forms.ToolStripMenuItem Menu_Tools;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help_Help;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help_About;
        private System.Windows.Forms.ToolStripMenuItem Menu_Tools_Explore;
        private System.Windows.Forms.ToolStripMenuItem Menu_Tools_ViewPdf;
        private System.Windows.Forms.ToolStripMenuItem Menu_Tools_ViewTiff;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem Menu_Tools_Options;
        private System.Windows.Forms.Label Lbl_WrkFolder;
        private System.Windows.Forms.TextBox Txt_WorkFolder;
        private System.Windows.Forms.ToolStripMenuItem advancedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Menu_Search;
        private System.Windows.Forms.ToolStripMenuItem Menu_Search_Find;
        private System.Windows.Forms.ToolStripMenuItem Menu_Search_FindInFiles;
        private System.Windows.Forms.ToolStripMenuItem Menu_Search_FindNext;
        private System.Windows.Forms.ToolStripMenuItem Menu_Search_FindPrevious;
        private System.Windows.Forms.ToolStripMenuItem Menu_Search_Replace;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem Menu_Search_GoTo;
        private System.Windows.Forms.Label Lbl_Main_Version;
        private System.Windows.Forms.Label Lbl_Command;
        private System.Windows.Forms.TextBox Txt_Cmd;
    }
}

