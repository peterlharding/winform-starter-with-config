﻿using System;
using System.Windows.Forms;

namespace Skel.WinForms
{
    public partial class ConfigForm : Form
    {
        private readonly Configuration _configuration;

        //------------------------------------------------------------------------------------

        public ConfigForm(Configuration configuration)
        {
            _configuration = configuration;

            InitializeComponent();
        }

        //------------------------------------------------------------------------------------

        private void ConfigForm_Load(object sender, EventArgs e)
        {
            Btn_SolidBrushColourPicker.BackColor = _configuration.SolidBrushColour; // Color.DarkSeaGreen;
            Btn_DottedBrushColourPicker.BackColor = _configuration.DottedBrushColour; // Color.DarkSeaGreen;

        }

        //------------------------------------------------------------------------------------

        private void Btn_SolidBrushColourPicker_Click(object sender, EventArgs e)
        {
            var dialog = new ColorDialog();

            dialog.AllowFullOpen = true;
            dialog.AnyColor = true;
            dialog.SolidColorOnly = false;
            dialog.Color = _configuration.SolidBrushColour; // Color.Red;

            var results = dialog.ShowDialog();

            if (results == DialogResult.OK)
            {
                Btn_SolidBrushColourPicker.BackColor = dialog.Color;
            }

            _configuration.SolidBrushColour = dialog.Color;


        }

        //------------------------------------------------------------------------------------

        private void Btn_DottedBrushColourPicker_Click(object sender, EventArgs e)
        {
            var dialog = new ColorDialog();

            dialog.AllowFullOpen = true;
            dialog.AnyColor = true;
            dialog.SolidColorOnly = false;
            dialog.Color = _configuration.DottedBrushColour; // Color.Red;

            var results = dialog.ShowDialog();

            if (results == DialogResult.OK)
            {
                Btn_DottedBrushColourPicker.BackColor = dialog.Color;
            }

            _configuration.DottedBrushColour = dialog.Color;
        }

        //------------------------------------------------------------------------------------



    }
}
