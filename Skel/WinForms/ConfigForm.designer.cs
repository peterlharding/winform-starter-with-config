﻿namespace Skel.WinForms
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Close = new System.Windows.Forms.Button();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.Lbl_SelColour = new System.Windows.Forms.Label();
            this.Btn_SolidBrushColourPicker = new System.Windows.Forms.Button();
            this.Btn_DottedBrushColourPicker = new System.Windows.Forms.Button();
            this.Grp__ColourPickers = new System.Windows.Forms.GroupBox();
            this.Grp__ColourPickers.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_Close
            // 
            this.Btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Btn_Close.Location = new System.Drawing.Point(493, 312);
            this.Btn_Close.Name = "Btn_Close";
            this.Btn_Close.Size = new System.Drawing.Size(75, 23);
            this.Btn_Close.TabIndex = 1;
            this.Btn_Close.Text = "Close";
            this.Btn_Close.UseVisualStyleBackColor = true;
            // 
            // Btn_OK
            // 
            this.Btn_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Btn_OK.Location = new System.Drawing.Point(412, 312);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(75, 23);
            this.Btn_OK.TabIndex = 2;
            this.Btn_OK.Text = "Save";
            this.Btn_OK.UseVisualStyleBackColor = true;
            // 
            // Lbl_SelColour
            // 
            this.Lbl_SelColour.AutoSize = true;
            this.Lbl_SelColour.Location = new System.Drawing.Point(15, 132);
            this.Lbl_SelColour.Name = "Lbl_SelColour";
            this.Lbl_SelColour.Size = new System.Drawing.Size(33, 13);
            this.Lbl_SelColour.TabIndex = 3;
            this.Lbl_SelColour.Text = "Other";
            // 
            // Btn_SolidBrushColourPicker
            // 
            this.Btn_SolidBrushColourPicker.Location = new System.Drawing.Point(6, 19);
            this.Btn_SolidBrushColourPicker.Name = "Btn_SolidBrushColourPicker";
            this.Btn_SolidBrushColourPicker.Size = new System.Drawing.Size(128, 23);
            this.Btn_SolidBrushColourPicker.TabIndex = 4;
            this.Btn_SolidBrushColourPicker.Text = "Solid Brush Colour";
            this.Btn_SolidBrushColourPicker.UseVisualStyleBackColor = true;
            this.Btn_SolidBrushColourPicker.Click += new System.EventHandler(this.Btn_SolidBrushColourPicker_Click);
            // 
            // Btn_DottedBrushColourPicker
            // 
            this.Btn_DottedBrushColourPicker.Location = new System.Drawing.Point(6, 48);
            this.Btn_DottedBrushColourPicker.Name = "Btn_DottedBrushColourPicker";
            this.Btn_DottedBrushColourPicker.Size = new System.Drawing.Size(128, 23);
            this.Btn_DottedBrushColourPicker.TabIndex = 5;
            this.Btn_DottedBrushColourPicker.Text = "Dotted Brush Colour";
            this.Btn_DottedBrushColourPicker.UseVisualStyleBackColor = true;
            this.Btn_DottedBrushColourPicker.Click += new System.EventHandler(this.Btn_DottedBrushColourPicker_Click);
            // 
            // Grp__ColourPickers
            // 
            this.Grp__ColourPickers.Controls.Add(this.Btn_SolidBrushColourPicker);
            this.Grp__ColourPickers.Controls.Add(this.Btn_DottedBrushColourPicker);
            this.Grp__ColourPickers.Location = new System.Drawing.Point(12, 12);
            this.Grp__ColourPickers.Name = "Grp__ColourPickers";
            this.Grp__ColourPickers.Size = new System.Drawing.Size(141, 83);
            this.Grp__ColourPickers.TabIndex = 6;
            this.Grp__ColourPickers.TabStop = false;
            this.Grp__ColourPickers.Text = "Select Colours";
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 347);
            this.Controls.Add(this.Grp__ColourPickers);
            this.Controls.Add(this.Lbl_SelColour);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.Btn_Close);
            this.Name = "ConfigForm";
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.ConfigForm_Load);
            this.Grp__ColourPickers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_Close;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Label Lbl_SelColour;
        private System.Windows.Forms.Button Btn_SolidBrushColourPicker;
        private System.Windows.Forms.Button Btn_DottedBrushColourPicker;
        private System.Windows.Forms.GroupBox Grp__ColourPickers;
    }
}